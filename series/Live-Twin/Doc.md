## ${Labrynth（拉比林斯）}$

>终端：``、``  
>启点：``  
>先后：0.1  
>$\color{red}{支援怪兽：}$  
>$\color{red}{支援魔法：}$`强欲之壶`、`贪欲之壶`、``、``、`谦虚之壶`、`天使的施舍`、``、``  
>$\color{red}{支援陷阱：}$  

* 直播双子是一套beat型卡组，依靠两位双子，姬丝基勒和璃拉（以下简称小红和小蓝）相互调度来增加自己的资源并且依靠大量手坑和红坑削减对手资源来获得胜利。因为有大量手坑，所以保证了在bo1里后攻也有一定胜率。小红和小蓝她俩共同效果都是场上没有别的怪兽时从卡组拉另外一位。小红的隐藏效果是对手攻击宣言，自己回复500LP。小蓝的隐藏效果是对手攻击宣言对面扣500LP。
* 除此之外二者还有皮肤，璃拉糖果和姬丝霜精，这两个的共同效果是场上有姬丝（璃拉）时可以手卡特招（不入连锁），是双子的补点。霜精可以在自己场上有邪恶双子时，对面把卡组里的卡加入手卡时除外，自己抽1，糖果可以在邪恶双子战伤时把这张卡除外，降低对手一只怪兽的攻击。
* 双子可以进行连接召唤对应的邪恶双子，邪恶双子共同效果是从墓地特殊召唤另一只双子，比如邪恶双子璃拉（大蓝）可以拉墓地的姬丝，而邪恶双子姬丝（大红）可以拉墓地的璃拉。这个墓地复活是二速的。大红另一个效果是场上有璃拉时特殊召唤成功可以抽1，大蓝另一个效果是场上有姬丝时特殊召唤成功可以取对象破坏一张卡。
* 双子的强度来自于一卡动和不停地抽卡。一卡动意味着动点多，抽卡是最直接的攒资源的方式，可以让卡组里的强力卡更快上手。任意小红和小蓝（不是霜精和糖果）就可以从卡组拉出对方，然后进行连接召唤出大红和大蓝。基础combo是小蓝优先拉霜精，link出大红，大红复活小蓝，link出大蓝，大蓝拉大红，大红效果抽1。这一套打完，一张手卡都没亏，这里你就比对面赚了一卡了，接下来大红大蓝link出大红。对手回合，对面启动时二速复活大蓝炸对面1卡，这样你就比对面赚了2张卡。如果对面检索了，你可以除外霜精，再抽1，卡差就开始慢慢累积。