## ${Eldland（黄金国）}$

>终端：`群豪之聚-幻中`  
>启点：`群豪之巫女-东云`  
>先后：0.1  
>$\color{red}{支援怪兽：}$``  
>$\color{red}{支援魔法：}$`强欲之壶`、`贪欲之壶`、`无欲之壶`、`谦虚之壶`、`天使的施舍`、`灵摆融合`、`位置移动`  
>$\color{red}{支援陷阱：}$`王宫的通告`、`颉颃胜负`  

* 目前这个系列出了十张怪兽卡，其中八张灵摆效果怪兽，两张灵摆融合怪兽。表面上看这是套灵摆卡组，因为怪兽卡全是灵摆怪兽，但其实这是个虚假的灵摆卡组，因为主卡组里的八只灵摆怪兽的刻度都是一样的，并且也没有改变刻度的效果。换而言之，这套卡组其实是没法进行灵摆召唤的，除非后期想办法把融合怪兽放到灵摆区域来，而这就还要等融合怪兽被破坏了。（群豪的融合怪兽刻度为10，和主卡组里的八只灵摆怪兽刻度1不同，但是要把融合怪兽放到灵摆区域就必须等到“群豪之聚-幻中”被破坏）。群豪系列的灵摆怪兽的灵摆效果几乎都是直接从魔法陷阱卡区域跳到怪兽卡区域的效果，所以这也是为什么这个系列还需要检索“位置移动”这张卡的缘故。（“位置移动”是张很有年代感的卡，效果是挪到怪兽的位置，除群豪外没有哪套卡组需要）群豪系列的怪兽效果大多数都是要在已特殊召唤的状态下才能发动；其他便是主动移动怪兽区域，以及以移动怪兽区域为触发条件发动的效果。场地魔法卡一旦发动便会把对面的场地区域也给占领，场地卡的效果能让怪兽从魔法陷阱卡区域移到怪兽区域，也能把怪兽从怪兽区域移回魔法陷阱卡区域。总的来讲，这套卡组打起来还是蛮有意思的，会像下棋一样将怪兽挪来挪去的。
* 群豪系统内没有陷阱卡，投入`王宫的通告`来压制神字反击陷阱和强力红坑贴纸，另外新大师规则下灵摆位会占据两个后场格子，用来放置永续魔法/陷阱卡的后场位置并不充裕。由于群豪卡组的后手突破能力较弱，因此投入了`颉颃胜负`用来解场。
* 群豪系统由炎属性机械族和水属性魔法师族两个风格迥然不同的怪兽系统组成，从卡图上看应该是neta了街机类的电子游戏，其中炎属性机械族的怪兽以中世纪欧洲的爵位（2星`男爵`、4星`子爵`、6星`侯爵`、8星`公爵`）进行命名；而水属性魔法师族的怪兽则以古代日本武职（2星巫女、4星弓引、6星忍者、8星武者）进行命名。两套子系统均拥有位置移动、放置摆子、特殊召唤的系统，但也具有各自独有的一些效果，水系统中`巫女`的卡组检索、`武者`的融合是炎系统中不具备的，而炎系统中`男爵`的将怪兽作为永续魔法置于后场、`公爵`的NTR也是水系统所不具备的。
* 群豪卡组的压制力由本家上级`幻中`提供，`幻中`能够将对方的怪兽推到后场来直接阻断对手的展开，在被破坏时还能变为摆子，再下回合继续登场，可以说集强势干扰和自我苏生于一体。卡组的核心目标应该是尽快做出`幻中`，最佳方法是使用武者的效果进行融合，这样融合素材会进入额外卡组而非墓地，能够被`男爵`和`群豪大战-初始之地`二次利用。