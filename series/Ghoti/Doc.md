## ${Ghoti（魊影）}$

>终端：  
>启点：  
>先后：  
>$\color{red}{支援怪兽：}$``  
>$\color{red}{支援魔法：}$`强欲之壶`、`贪欲之壶`、`大欲之壶`、`无欲之壶`、`谦虚之壶`、`天使的施舍`、`一击必杀！居合抽卡`、`苦涩的选择`  
>$\color{red}{支援陷阱：}$魔兽的大饵  

* 魊影系统的设计逻辑有点像玄化系统，都是需要将怪兽除外来发动效果，所以都会考虑投入大宇宙和次元的裂缝，不同之处主要在于展开的方法。魊影是一套披着同调外衣的回合外展开型卡组，本家下级在被除外后的对手回合可以返回场上加速同调，同时发动登场效果来干扰对方。
* 本家星级最高终端深远魊影一般负责执行OTK的最后一击，但其需要在除外区(双方)积累怪兽才能形成高攻击力。白斗气外挂在魊影系统中可以起到不错的补强作用。
* 除了依赖本家下级怪兽展开外，魊影系统还有一张能够保底本家场地。这张卡理论上可以将所有本家怪兽送去除外区。