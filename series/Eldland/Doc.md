## ${Eldland（黄金国）}$

>终端：`黄金狂-黄金国巫妖`  
>启点：黄金国永生药系列魔法陷阱  
>先后：0.4  
>$\color{red}{支援怪兽：}$`灰流丽`、`屋敷童`、`冥界龙-龙亡`、`巨骸龙-闪耀`、`星态龙`、`鲜花女男爵`、`冥界浊龙-龙叹`、`白斗气双头神龙`、`流星龙`、`神树的守护兽-牙王`、`超重型炮塔列车-古斯塔夫最大炮`、`超重型炮塔列车-破天巨爱`、`闭锁世界的冥神`  
>$\color{red}{支援魔法：}$`强欲之壶`、`大欲之壶`、`贪欲之壶`、`无欲之壶`、`谦虚之壶`、`天使的施舍`、`不死世界`、`超融合`、`突然变异`  
>$\color{red}{支援陷阱：}$`技能抽取`、`御前试合`、`颉颃胜负`、`召唤限制器`  

* `黄金卿-黄金国巫妖`的效果是在墓地发动，不会受到`技能抽取`的影响因而相性极佳。对于一些不取对象的终端（`超魔导龙骑士`、`双穹之骑士`），黄金国卡组体系内的处理办法不多，一般需要支援卡片来解场。可行的方法有：（1）使用`技能抽取`无效化这些终端的效果，然后用黄金狂的高攻击力战斗破坏；（2）使用`谦虚之壶`令其返回卡组；（3）发动`不死世界`改变其种族，然后用`超融合`解场；（4）积累4只怪兽资源将其一起做为素材链接召唤`冥神`
* 黄金国卡组不惧怕增殖的G，第一回合一般只在自己的回合盖后场，到对方回合时才发动效果特招巫妖。并且本家的魔法陷阱卡送墓后还能再检索另一张本家魔陷盖到场上，其运转十分流畅，即便是60卡的大卡组也很少发生卡手的情况。
* 卡组运转思路就是围绕`黄金国巫妖`用本家的红坑和泛用贴纸进行压制，最惧怕的是`黄金国巫妖`被除外，因此投入`大欲之壶`和`无欲之壶`来回收被除外的`巫妖`和发动效果被除外的本家魔陷。