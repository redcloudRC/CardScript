--謙虚な壺
function c70278545.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_TODECK)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(c70278545.target)
	e1:SetOperation(c70278545.activate)
	c:RegisterEffect(e1)
	-- --Activate
	-- local e1=Effect.CreateEffect(c)
	-- e1:SetDescription(aux.Stringid(70278545,0))
	-- e1:SetCategory(CATEGORY_TODECK)
	-- e1:SetType(EFFECT_TYPE_ACTIVATE)
	-- e1:SetCode(EVENT_FREE_CHAIN)
	-- e1:SetHintTiming(0,TIMING_END_PHASE)
	-- e1:SetCost(c70278545.cost)
	-- e1:SetTarget(c70278545.target)
	-- e1:SetOperation(c70278545.activate)
	-- c:RegisterEffect(e1)
end
-- function c70278545.target(e,tp,eg,ep,ev,re,r,rp,chk)
	-- if chk==0 then return Duel.IsExistingMatchingCard(Card.IsAbleToDeck,tp,LOCATION_HAND,0,2,e:GetHandler()) end
	-- Duel.SetTargetPlayer(tp)
	-- Duel.SetOperationInfo(0,CATEGORY_TODECK,nil,2,tp,LOCATION_HAND)
-- end
-- function c70278545.activate(e,tp,eg,ep,ev,re,r,rp)
	-- local p=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER)
	-- local g=Duel.GetMatchingGroup(Card.IsAbleToDeck,p,LOCATION_HAND,0,nil)
	-- if g:GetCount()>=2 then
		-- Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TODECK)
		-- local sg=g:Select(p,2,2,nil)
		-- Duel.SendtoDeck(sg,nil,SEQ_DECKSHUFFLE,REASON_EFFECT)
	-- end
-- end
c70278545.rchecks=aux.CreateChecks(Card.IsAttribute,{ATTRIBUTE_EARTH,ATTRIBUTE_WATER,ATTRIBUTE_FIRE,ATTRIBUTE_WIND})
function c70278545.rfilter(c)
	return c:IsAttribute(ATTRIBUTE_EARTH+ATTRIBUTE_WATER+ATTRIBUTE_FIRE+ATTRIBUTE_WIND)
		and c:IsType(TYPE_MONSTER) and c:IsAbleToRemoveAsCost()
end
function c70278545.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(Card.IsAbleToDeck,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,e:GetHandler()) end
	local g=Duel.GetMatchingGroup(Card.IsAbleToDeck,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,e:GetHandler())
	Duel.SetOperationInfo(0,CATEGORY_TODECK,g,1,0,0)
end
function c70278545.activate(e,tp,eg,ep,ev,re,r,rp)
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(tp,Card.IsAbleToDeck,tp,LOCATION_ONFIELD,LOCATION_ONFIELD,1,1,aux.ExceptThisCard(e))
	if g:GetCount()>0 then
		Duel.HintSelection(g)
		Duel.SendtoDeck(g,nil,SEQ_DECKSHUFFLE,REASON_EFFECT)
		----------------------------------------------------
		Duel.BreakEffect()
		Duel.Draw(tp,1,REASON_EFFECT)
	end
end