## ${Snowflower（六花）}$

>终端：`六花圣-泪滴花束雪花莲`、`神树兽-许珀利冬`  
>启点：`六花的一瓣`、`六花来来`  
>先后：0.2  
>$\color{red}{支援怪兽：}$`神树兽-许珀利冬`、'雪月花美神-月下美人'  
>$\color{red}{支援魔法：}$`强欲之壶`、`贪欲之壶`、`谦虚之壶`、`无欲之壶`、`天使的施舍`、`一击必杀！居合抽卡`、`拼图之圈`  
>$\color{red}{支援陷阱：}$`常春藤束缚`、`花粉症`、`颉颃胜负`  

* 六花是一套依靠解放怪兽来进行展开的植物族卡组，进可无敌展爆场，退可摆烂续航强。六花卡组的主要优势是对方的前场会跟纸糊的一样脆弱，而主要劣势是完全无法处理对方后场。因而，在目前以前场压制为核心的环境中发挥良好。在传统构筑中，六花一般会依靠保姆级的植物族外挂`芳香炽天使茉莉`或者圣天树轴提供续航和终场强度，卡组增强后六花本家的系统强度已经足够，特别是六花来来赋予的可以把对方怪兽作为cost的赖皮效果，使得六花卡组几乎拥有取之不尽、随叫随到的海龟坏兽，轻而易举就能将对方花费巨大资源的终端解掉。
* 六花的运转核心不是任何一张单卡，而是本家系统通过解放cost展开的先进机制。六花的一瓣能够提供从卡组中无限获得怪兽的能力，而六花来来的主动效果则是无限获得卡组中的魔陷资源（六花来来需要有本家怪兽在场）。`六花圣-泪滴花束雪花莲`看似效果不强，但却是本家系统内唯一一只能够主动开解放cost的怪兽，在有六花来来时可以解放对方的怪兽，2素材可以解掉2只对方终端，同时还能在场上有怪兽解放时提升自己回合内的攻击力，解场和压制能力都足够强。神树兽-许珀利冬作为外挂终端，主要承担怪兽康的任务，其登场需要依靠六花圣-力量女神花圈做为跳板。
* 尽管六花本家怪兽不少都拥有墓效，但其完全不依赖墓地资源，墓地中的六花下级主要提供卡组续航，所有展开都只消耗卡组中的资源。因此六花不需要堆墓卡，反而需要大量回收墓地资源的卡支持，比如`贪欲之壶`、`无欲之壶`等。