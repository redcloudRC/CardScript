## ${DragonRuler（征龙）}$

>终端：  
>启点：  
>先后：0.5  
>$\color{red}{支援怪兽：}$  
>$\color{red}{支援魔法：}$`强欲之壶`、`贪欲之壶`、`大欲之壶`、`无欲之壶`、`谦虚之壶`、`天使的施舍`、`一击必杀！居合抽卡`、`苦涩的选择`  
>$\color{red}{支援陷阱：}$  

* 征龙不是正式的系列字段。怪兽分成两个部分，3星和4星的下级龙族怪兽以及7星的上级龙族怪兽，分别代表地·水·炎·风4个属性。效果支援大部分龙族怪兽以及相同属性的卡组系列，因为强力的泛用效果以及普遍的配合率，除了风龙外其他征龙至今仍是禁止卡。全盛期征龙卡组拥有强大特召能力（能从卡组快速拉大征龙）和检索能力，并且拥有堆墓+检索效果的7星龙族怪兽。一回合中可以叠出`幻兽机-哥萨克龙`、`No.11-巨眼`等强力R7，其资源运转能力和效率在历代主流中位居前列。比较夸张地说，征龙的手牌是卡组，卡组是手牌，墓地是手牌，除外区也是手牌，几乎没有无法展开的局面。
* 修改后的征龙去除了一回合一次的限制，使得大小龙之间的资源调动空前灵活，由此产生了极高的过牌量，配合`七星宝刀`、`强欲之壶`、`天使的施舍`这些滤抽卡，在一回合内抽半套卡组是很常见的情况。但征龙卡组也并非毫无弱点，受限于当时的设计理念还不够先进，其防坑防阻断的方法有限，特别是在开始的一两个回合，资源还没有运转起来时，一旦被`灰流丽`、`墓穴指名者`、`神之通告`这类的坑打断特招，很可能因为无法在墓地积累足够的资源而导致展开失败，而这类情况在对抗幻变骚灵这类会携带大量打断卡的坑beat卡组时尤为严重。
* 征龙本家没有终端，主要依赖外挂R7超量进行战斗，纯化卡组后很多优秀的R7超量无法投入，一般考虑使用的终端有：。